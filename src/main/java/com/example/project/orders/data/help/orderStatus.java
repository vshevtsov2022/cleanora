package com.example.project.orders.data.help;

public enum orderStatus {
    CREATED,CANCELED,CLEANER_APPOINTED,ON_THE_WAY,HERE,STARTED,FINISHED,PAID
}
